class RelationshipMailerPreview < ActionMailer::Preview

  # Preview all emails at http://localhost:3000/rails/mailers/relationship_mailer/follow_notification
  def follow_notification
    user = User.first
    follower = User.second
    RelationshipMailer.follow_notification(user, follower)
  end
  
  # Preview all emails at http://localhost:3000/rails/mailers/relationship_mailer/unfollow_notification
  def unfollow_notification
    user = User.first
    follower = User.second
    RelationshipMailer.unfollow_notification(user, follower)
  end
end
