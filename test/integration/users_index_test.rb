require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @non_admin = users(:archer)
  end

  test "index including pagination and delete links" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end
  
  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delte', count: 0
  end
  
  # 演習11.40課題
  # test "should not view nonactivated user" do
  # end
    
  test "users search" do
    log_in_as(@admin)
    # すべてのユーザーを表示する
    get users_path, params: {q: {name_cont: "" } }
    User.paginate(page:1).each do |user|
    assert_select 'a[href=?]', user_path(user), text:user.name
    end
    assert_select 'title', "All users | Ruby on Rails Tutorial Sample App"
    # ユーザー検索
    get users_path, params: { q: { name_cont: "a" } }
    q = User.ransack(name_cont: "a", activated_true: true)
    q.result.paginate(page:1).each do |user|
      assert_select 'a[href=?]', user_path(user), text:user.name
    end
    assert_select 'title', "Search Result | Ruby on Rails Tutorial Sample App"
    # ユーザー検索（検索結果無し）
    get users_path, params: { q: { name_cont: "abcdefghijk" } }
    assert_match "Couldn't find any user.", response.body
    # すべのユーザーに戻る
    get users_path, params: { q: { name_cont: "" } }
    assert_select 'title', "All users | Ruby on Rails Tutorial Sample App"
  end
end